# Welcome to our GitLab Repository

Services like Slack changed the way people, especially businesses, communicate. Mattermost took it a step further by allowing users to bring the service in-house. And yet, none of the services in this category has yet to release a decent native app. Both Slack and Mattermost, along with a host of other popular platforms, chose to release their official apps as little more than a Node wrapper around their webapp. While this _does_ allow for a quick turnaround on development, and allows the app to be cross-platform with little to no effort, it also results in users having to run yet another glorified web browser, with all the headaches, inconsistencies and bottlenecks that come along with it. Silva is an attempt to break that standard by creating a truly native Mattermost client using Rust and GTK3.

## Warning: Here be Dragons

Before you get too excited, I should make a few things clear. First, while I am no stranger to development, I am far from an expert on Rust. This is my first truly major project in Rust, and my first project period combining Rust and GTK. I'm learning as I go here, folks. There _will_ be broken things (for a while), there _will_ be less-than-ideal logic patterns (hopefully just for a while), and I will likely write and re-write things a dozen different ways before they are even _close_ to what will be in the first official public release.

However... there's good news too. I'm stubborn (in a good way) so, eventually, this project _will_ achieve some semblance of stability. Also, I really, truly, 100% believe in open source, so I not only welcome, but encourage [user contributions](https://gitlab.com/pop-planet/silva/blob/master/CONTRIBUTING.md). If you think you have something to teach me, feel free to let me know (you know, open an issue or a PR). Just... try not to rewrite the entire thing in two days. This is my little educational pet project, so I'd like to actually get some education out of it. Patience is a virtue.

## Why Silva?

If you've been following since the beginning, you're probably aware of the struggles we faced naming this project. For quite a while, it was simply referred to as "MMC" because I personally (@evertiro) have precisely zero ability to quickly choose project names; I put way too much thought into them. Unfortunately, MMC just stood for "Mattermost Client" which is both incredibly unimaginative and a legal nightmare since "Mattermost" is trademarked and their TOS precludes it being the first word in the name of any third-party app.

We spent much longer than we should have trying to boil down the concepts of rust, matter, communication, and nature down to something that a) was easy to say, b) wasn't already a registered trademark in the same industry (we have no immediate interest in registering Silva, but didn't want to risk having a run-in with someone down the road either) and c) was relevant, even peripherally.

We finally settled on _Silva_. Silva is a Latin word which, commonly, is used to define the trees in a specific area or region. Helpfully, it can also less commonly be directly translated to _matter_. In our abstraction, each Mattermost server is a comprised of a group of users in a specific entity, much like the trees that comprise a specific forest.

## Installation

1. Clone the GitLab repository: `https://gitlab.com/pop-planet/silva.git` or [download a ZIP file](https://gitlab.com/pop-planet/silva/-/archive/master/silva-master.zip)
2. `make && sudo make install` (Yep, I know there's no makefile yet. It wouldn't build yet anyway. Fetch the deps yourself or be patient.)

### Dependencies

* cargo (Rust 1.28.0) (Note to self, don't forget to run backwards compatibility tests)
* libgtk-3-dev
* I'm sure there will be more... I just don't know what they are right now

## Bugs

If you find an issue, please let us know [here](https://gitlab.com/pop-planet/silva/issues)!

## Contributions

Anyone is welcome to contribute. Please read the [guidelines for contributing](https://gitlab.com/pop-planet/silva/blob/master/CONTRIBUTING.ms).